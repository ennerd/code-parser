<?php
require("../vendor/autoload.php");

use function F2\asserty;

$parser = new F2\CodeParser\CodeParser();

$result = $parser->parse($original = file_get_contents(__FILE__));


function build($nodes) {
    $result = '';
    foreach ($nodes as $node) {
        switch($node[0]) {
            case '(':
                $result .= '('.build($node[1]).')';
                break;
            case '{':
                $result .= '{'.build($node[1]).'}';
                break;
            case '[':
                $result .= '['.build($node[1]).']';
                break;
            default:
                $result .= $node[1];
                break;
        }
    }
    return $result;
}



asserty($original === build($result), "Rebuilt is different from original");
