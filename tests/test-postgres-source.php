<?php
require("../vendor/autoload.php");

use function F2\asserty;

$parser = new F2\CodeParser\CodeParser(['stripWhitespace' => true]);

foreach($parser->parse(file_get_contents("../sources/PostgresAdapter.php")) as $node) {
    $parsed[] = $node;
}
echo json_encode($parsed, JSON_PRETTY_PRINT);
