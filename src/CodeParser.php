<?php
namespace F2\CodeParser;

class CodeParser {

    public $options = [
        /**
         * Emitted as [ 'token', '...' ]
         * By default, all characters that don't match any config are emitted as 'token'.
         * If the last character in the token is alphanumeric, then the match will fail if the next character in the source is also alphanumeric
         */
        'tokens' => [
            '<?php',
            '?>',
            '!==',
            '+=',
            '-=',
            '*=',
            '/=',
            '&=',
            '->',
            '.=',
            '...',
            '::',
            '<==',
            '===',
            '>==',
            '=>',
            '^=',
            '~=',
            '<>',
            '!=',
            '&&',
            '**',
            '++',
            '--',
            '<=',
            '==',
            '>=',
            '||',
            '<<',
            '>>',
            '>>>',
            '<=>',
            '??',
            '?=',
            '**=',
            '%=',
            '<<=',
            '>>=',
        ],
        'comments' => [
            "/*" => "*/",
            "#" => "\n",
            "//" => "\n",
        ],
        'quotes' => [
            "'" => "'",
            '"' => '"',
        ],
        'doubleQuoteEscape' => false,                   // some languages use "" instead of \" for escaping strings

        'hexChars' => '0123456789abcdefABCDEF',
        'decChars' => '0123456789',
        'binChars' => '01',

        'alphabetical'  => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPWRSTUVWXYZ_',
        'numerical'     => '0123456789',

        'numberIllegalChars' => 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPWRSTUVWXYZ_',

        'literalConstants' => [
            'true' => true,
            'false' => false,
            'null' => null,
            ],

        'stripComments' => false,
        'stripWhitespace' => false,

        'scanners' => ['whitespace', 'bracket', 'identifier', 'comment', 'token', 'literal-number', 'literal-string', 'literal-constants', 'single-token'],
    ];

    public function __construct(array $options=null) {
        if ($options) {
            $this->options = $options + $this->options;
        }
    }

    public function parse($code) {
        $options = &$this->options;

        // precompile tokens for faster searching
        $options['_tokens'] = [];

        foreach($options['tokens'] as $token) {
            $tl = strlen($token);
            $options['_tokens_alnum'][$token] = ctype_alnum($token[$tl-1]);
            $options['_tokens'][$token[0]][$tl][] = $token;
        }
        foreach ($options['_tokens'] as $c => &$tokens) {
            krsort($tokens);
        }

        $scanners = [
            'any' => function($code, $l, &$i) use (&$options, &$scanners) {
                if ($i >= $l) {
                    return null;
                }
                do {
                    $start = $i + 0;
                    foreach ($options['scanners'] as $scanner) {
                        $res = $scanners[$scanner]($code, $l, $i);
                        if ($res !== null) {
                            return $res;
                        }
                    }
                } while($i !== $start);
                throw new \Exception("$start $i $l No scanners could be used for remaining code: ".substr($code, $i, 20));
            },
            'comment' => function($code, $l, &$i) use (&$options) {
                $start = $i;
                foreach($options['comments'] as $open => $close) {
                    if ($code[$i] == $open[0]) {
                        if (substr($code, $i, strlen($open)) === $open) {
                            $closes = strpos($code, $close, $i);
                            if ($closes === false) {
                                if ($close == "\n") {
                                    $close = "\\n";
                                }
                                throw new Exception("'$open' without '$close'");
                            }
                            $i = $closes + strlen($close);
                            if ($options['stripComments']) {
                                return false;
                            }
                            return [ 'comment', substr($code, $start, $i - $start) ];
                        }
                    }
                }
                return null;
            },
            'literal-constants' => function($code, $l, &$i) use(&$options) {
                foreach($options['literalConstants'] as $word => $value) {
                    if ($code[$i] === $word[0]) {
                        if (substr($code, $i, $wl = strlen($word)) === $word) {
                            $i += $wl;
                            return [ 'literal', $value ];
                        }
                    }
                }
                return null;
            },
            'literal-hex' => function($code, $l, &$i) use (&$options) {
                if ($i+2 < $l && $code[$i]=='0' && $code[$i+1]=='x') {
                    $i += 2;
                    $start = $i;
                    while ($i < $l && trim($code[$i], $options['hexChars'])) {
                        $i++;
                    }
                    if ($start === $i) {
                        throw new Exception("Literal needs some numbers");
                    }
                    return [ 'literal', hexdec(substr($code, $start, $i - $start)) ];
                }
            },
            'literal-bin' => function($code, $l, &$i) use (&$options) {
                if ($i+2 < $l && $code[$i]=='0' && $code[$i+1]=='x') {
                    $i += 2;
                    $start = $i;
                    while ($i < $l && trim($code[$i], $options['binChars'])) {
                        $i++;
                    }
                    if ($start === $i) {
                        throw new Exception("Literal needs some numbers");
                    }
                    return [ 'literal', bindec(substr($code, $start, $i - $start)) ];
                }
            },
            'literal-number' => function($code, $l, &$i) use (&$scanners, &$options) {
                $start = $i;
/**
 * stage 0      Expect the first symbol
 */
                if ($code[$i]=='0') {
                    if ($i + 1 < $l) {
                        $i++;
                        return [ 'literal', 0 ];
                    } elseif ($code[$i+1] == 'x') {
                        return $scanners['literal-hex'];
                    } elseif ($code[$i+1] == 'b') {
                        return $scanners['literal-bin'];
                    } elseif ($code[$i+1] != '.') {
                        throw new Exception("Can't parse '".substr($code, $i, 2));
                    }
                }
                $start = $i;
                while ($i < $l && trim($code[$i], $options['decChars'])==='') {
                    $i++;
                }
                if ($i === $start) {
                    return null;
                }
                if ($i === $l) {
                    return [ 'literal', intval(substr($code, $start)) ];
                }
                if (trim($code[$i], $options['numberIllegalChars'])==='') {
                    throw new Exception("Invalid identifier '".substr($code, $start, $i - $start)."'");
                }
                if ($code[$i] !== '.') {
                    return [ 'literal', intval(substr($code, $start, $i - $start)) ];
                }
                $dot = $i++;
                while ($i < $l && trim($code[$i], $options['decChars'])==='') {
                    $i++;
                }
                if ($dot === $i-1) {
                    throw new Exception("Number expected after '.'");
                }
                if (trim($code[$i], $options['numberIllegalChars'])==='') {
                    throw new Exception("Invalid identifier '".substr($code, $start, $i - $start)."'");
                }
                return [ 'literal', floatval(substr($code, $start, $i - $start)) ];
            },
            'token' => function($code, $l, &$i) use (&$options) {
                // Early exit
                if (!isset($options['_tokens'][$code[$i]])) {
                    return null;
                }

                foreach ($options['_tokens'][$code[$i]] as $len => $tokens) {
                    $check = substr($code, $i, $len);
                    foreach ($tokens as $token) {
                        if ($check === $token && (!$options['_tokens_alnum'][$token] || !ctype_alnum($code[$i + $len]))) {
                            $i += $len;
                            return [ 'token', $token ];
                        }
                    }
                }

                return null;
            },
            'single-token' => function($code, $l, &$i) {
                if (!ctype_alnum($code[$i]) && ctype_print($code[$i])) {
                        $i++;
                        return [ 'token', $code[$i-1] ];
                }
                return null;
            },
            'bracket' => function($code, $l, &$i, $pairs = [ "(" => ")", "[" => "]", "{" => "}"]) use (&$scanners) {
                if (!isset($pairs[$code[$i]])) {
                    return null;
                }

                $res = [$code[$i], []];
                $close = $pairs[$code[$i]];
                $i++;
                while ($i < $l) {
                    $c = $code[$i];
                    if ($code[$i] == $close) {
                        $i++;
                        return $res;
                    }
                    if ($i < $l && false !== ($child = $scanners['any']($code, $l, $i))) {
                        $res[1][] = $child;
                    }
                }
                throw new Exception("EOF searching for '$close'.");
            },
            'identifier' => function($code, $l, &$i) {
                if (strpos('$abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_', $code[$i])===false) {
                    return null;
                }
                $start = $i;
                $i++;
                while ($i < $l) {
                    if (!ctype_alnum($code[$i])) {
                        return [ 'identifier', substr($code, $start, $i-$start) ];
                    }
                    $i++;
                }
                return [ 'identifier', substr($code, $start) ];
            },
            'literal-string' => function($code, $l, &$i) {
                if ($code[$i] !== '"' && $code[$i] !== "'") {
                    return null;
                }
                $until = $code[$i];
                $start = $i++;
                while ($i < $l) {
                    if ($code[$i] == $until && ($code[$i-1]!="\\" || $code[$i-2]=="\\")) {
                        return [ 'literal', substr($code, $start, -$start+ ++$i) ];
                    }
                    $i++;
                }
                throw new Exception("EOF inside quoted string");
            },
            'whitespace' => function($code, $l, &$i) use (&$options) {
                if (!\ctype_space($code[$i])) {
                    return null;
                }
                $start = $i;
                while (++$i < $l && \ctype_space($code[$i]));
                if ($this->options['stripWhitespace']) {
                    return false;
                }
                return [ 'whitespace', substr($code, $start, $i - $start) ];
            }
        ];

        $l = strlen($code);
        $i = 0;
        while ($i < $l) {
            $res = $scanners['any']($code, $l, $i);
            if ($res !== false) {
                yield $res;
            }
        }
    }
}
